const drive_popup_script = async () => {
    await wasm_bindgen(browser.runtime.getURL("./wasms/bindgens.wasm"));
    wasm_bindgen.popup()
}
drive_popup_script();
