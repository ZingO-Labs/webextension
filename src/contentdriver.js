const drive_content_script = async () => {
    await wasm_bindgen(browser.runtime.getURL("./wasms/bindgens.wasm"));
    wasm_bindgen.content()
}
drive_content_script();
