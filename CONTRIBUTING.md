**CONTRIBUTING.md**
*this* is simply a *placeholder* file for where our code standards for this project will be.
<h1>so far we have linked to:</h1>
* [Crockford](http://crockford.com/javascript/code.html)
* Linting Docs
  * [Wikipedia](https://en.wikipedia.org/wiki/JSLint)
  * [Crockford's JSLint github repo](https://github.com/douglascrockford/JSLint)
  * [JSLint's Homepage](https://www.jslint.com)

That is all.
