#[macro_export]
macro_rules! mock_in_test {
    ($use_name:ident) => {
        cfg_if::cfg_if! {
                if #[cfg(test)] {
                    use self::mock::$use_name;
                } else {
                    use $use_name;
                }
        }
    };
}
