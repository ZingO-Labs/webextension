This tool facilitates micropayment driven anonymous curation of search results. 

We have a convention that the From and Into traits are side-effect free. This 
is likely Rust standard, but does not seem to be explicitly stated elsewhere.
