//! This module handles tasks that need to interact with a web-page.
//! Included functionality:
//!    * dom manipulation
//!    * receipt of message from the background
//!    * send messages to the background
//!
//! Data are represented with named field struct that transform into each other
//! using from.
use wasm_bindgen::closure::Closure;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_futures::JsFuture;
use wasm_bindgen_macro::wasm_bindgen;
use web_sys;
mod curationdb;
use cfg_if;

mod mock;
use crate::mock_in_test;
mock_in_test!(web_extension);

struct DDGResultsElement {
    results_element: web_sys::Element,
}

impl DDGResultsElement {
    fn from_document(document: &web_sys::Document) -> Self {
        DDGResultsElement {
            results_element: document
                .get_element_by_id("links")
                .expect("results element does not exist"),
        }
    }
    fn into_search_results(&self) -> SearchResults {
        let mut results = Vec::new();
        for i in 0..self.results_element.children().length() {
            if self
                .results_element
                .children()
                .item(i)
                .expect("results length is shorter than expected!")
                .id()
                .starts_with("r1-")
            {
                results.push((
                    self.results_element.children().item(i).expect(
                        "Results shorter than expected, shouldn't be possible!",
                    ),
                    0,
                ));
            }
        }
        SearchResults { results }
    }
}

struct SearchResults {
    results: Vec<(web_sys::Element, u32)>,
}

impl SearchResults {
    fn into_weighted_results(mut self) -> WeightedResults {
        let local_db = curationdb::load_curation_db();
        let mut weighted = Vec::new();
        self.results.drain(..).for_each(|(element, _zero)| {
            let weight = match local_db.get(&find_url(&element)) {
                Some(value) => {
                    add_zingo_icon(&element);
                    *value as u32
                }
                None => 0,
            };
            weighted.push((element, weight));
        });
        WeightedResults { weighted }
    }
}

struct WeightedResults {
    weighted: Vec<(web_sys::Element, u32)>,
}

impl WeightedResults {
    fn into_ordered_results(mut self) -> WeightedOrderedResults {
        self.weighted
            .sort_by(|(_e1, weight1), (_e2, weight2)| weight2.cmp(weight1));
        WeightedOrderedResults {
            weight_ordered: self
                .weighted
                .drain(..)
                .map(|(element, _weight)| element)
                .collect(),
        }
    }
}

struct WeightedOrderedResults {
    weight_ordered: Vec<web_sys::Element>,
}

fn get_window_and_document() -> (web_sys::Window, web_sys::Document) {
    let window = web_sys::window().expect("no global window object");
    let document = window.document().expect("window has no document");
    (window, document)
}

#[wasm_bindgen]
pub async fn content() {
    console_error_panic_hook::set_once();
    let runtime = &(*web_extension::browser).runtime();
    get_zaddr(runtime).await;
    send_str_to_background(runtime, "active").await;
    let dom_manip = move || {
        run_reordering_logic();
    };
    let (window, document) = get_window_and_document();
    match document.ready_state().as_str() {
        "complete" => dom_manip(),
        _ => window.set_onload(Some(
            Closure::once_into_js(dom_manip).as_ref().unchecked_ref(),
        )),
    };
    ()
}

fn find_url(element: &web_sys::Element) -> String {
    element
        .first_child_of_class("result__body links_main links_deep")
        .first_child_of_class("result__extras js-result-extras")
        .first_child_of_class("result__extras__url")
        .text_content()
        .expect("No url on element")
}

trait ChildrenByClass {
    fn first_child_of_class(&self, class: &str) -> web_sys::Element;
}

impl ChildrenByClass for web_sys::Element {
    fn first_child_of_class(&self, class: &str) -> web_sys::Element {
        for child in 0..self.children().length() {
            if self
                .children()
                .item(child)
                .expect("child list shorter than expected")
                .class_name()
                == class
            {
                return self
                    .children()
                    .item(child)
                    .expect("Child removed during execution");
            }
        }
        panic!("No child of class {}", class);
    }
}

fn append_reordered_results_to_dom(
    ddg_results_element: web_sys::Element,
    reordered_results: Vec<web_sys::Element>,
) {
    reordered_results.iter().for_each(|x| {
        ddg_results_element
            .append_child(&x)
            .expect("Child appending failed");
    });
    ddg_results_element
        .append_child(
            &ddg_results_element
                .children()
                .named_item("rld-1")
                .expect("No \"More Results\" element"),
        )
        .expect("failed to append child");
}

async fn send_str_to_background(
    runtime: &web_extension::Runtime,
    message: &str,
) {
    match JsFuture::from(runtime.send_message(
        None,
        &JsValue::from_str(message),
        None,
    ))
    .await
    {
        Ok(x) => web_sys::console::log_2(&JsValue::from("ok"), &x),
        Err(e) => {
            web_sys::console::log_2(&JsValue::from("err"), &JsValue::from(e))
        }
    };
}

async fn get_zaddr(runtime: &web_extension::Runtime) {
    let ear = Closure::wrap(Box::new(move |message: &JsValue| {
        web_sys::console::log_1(message);
    }) as Box<dyn Fn(&JsValue)>);
    runtime
        .on_message()
        .add_listener(ear.as_ref().unchecked_ref());
    ear.forget(); // If only we could make a consuming FFI!
}

// If you want "document.createElement" functionality, you can call this
// (for the default use):
// https://developer.mozilla.org/en-US/docs/Web/API/Document/createElement
fn get_html_element(tag: &str) -> wasm_bindgen::JsValue {
    let (_, document) = get_window_and_document();
    JsValue::from(
        document
            .create_element(tag)
            .expect("couldn't create element!"),
    )
}

fn add_zingo_icon(element: &JsValue) {
    let runtime = &(*web_extension::browser).runtime();
    let icon = web_sys::HtmlMediaElement::from(get_html_element("img"));
    icon.set_src(
        runtime
            .get_url("icons/zingo32px.png")
            .as_string()
            .expect("not a string")
            .as_str(),
    );
    icon.set_attribute("height", "32")
        .expect("failed to set height");
    icon.set_attribute("width", "32")
        .expect("failed to set width");
    web_sys::Element::from(element.clone())
        .append_child(&icon)
        .expect("failed to append child!");
}

fn run_reordering_logic() {
    let (_, document) = get_window_and_document();
    let reordered_results = DDGResultsElement::from_document(&document)
        .into_search_results()
        .into_weighted_results()
        .into_ordered_results();
    append_reordered_results_to_dom(
        DDGResultsElement::from_document(&document).results_element,
        reordered_results.weight_ordered,
    );
}

#[cfg(test)]
mod tests {
    use wasm_bindgen_test::{wasm_bindgen_test, wasm_bindgen_test_configure};
    wasm_bindgen_test_configure!(run_in_browser);
    use super::web_extension as mock_web_extension;
    use super::{get_zaddr, send_str_to_background};

    #[wasm_bindgen_test]
    async fn test_get_zaddr() {
        get_zaddr(&(*mock_web_extension::browser).runtime()).await;
    }

    #[wasm_bindgen_test]
    async fn test_send_message_to_background() {
        send_str_to_background(
            &(*mock_web_extension::browser).runtime(),
            "Sendthisstring",
        )
        .await;
    }
}
