extern crate wasm_bindgen_test;
use wasm_bindgen_test::wasm_bindgen_test_configure;
use wasm_bindgen_test::*;
use zingo_webextension::background;
wasm_bindgen_test_configure!(run_in_browser);

#[wasm_bindgen_test]
fn test() {
    assert_eq!(background::foo(), 1);
    //    background::background();
    //    background::get_runtime();
}
