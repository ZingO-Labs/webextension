use wasm_bindgen::closure;
use wasm_bindgen::{JsCast, JsValue};
use wasm_bindgen_futures::JsFuture;
use wasm_bindgen_macro::wasm_bindgen;
use web_extension;
use web_sys;

mod zcashclient;

use std::ops;
struct MessageReceiver {
    callback: closure::Closure<dyn ops::Fn(JsValue, JsValue)>,
}

impl MessageReceiver {
    fn new() -> Self {
        Self {
            callback: closure::Closure::wrap(Box::new(
                move |message: JsValue, sender: JsValue| {
                    web_sys::console::log_2(
                        &JsValue::from("received"),
                        &message,
                    );
                    web_sys::console::log_2(&JsValue::from("sender:"), &sender);
                    let m: String = message
                        .as_string()
                        .expect("Message can not be read as string");
                    if m.as_str() == "active" {
                        MessageReceiver::send_address_to_tabs(sender);
                    } else {
                        web_sys::console::log_1(&JsValue::from(
                            "Not a recognized input",
                        ));
                    }
                },
            )
                as Box<dyn Fn(JsValue, JsValue)>),
        }
    }
    fn get_console_script_channel() -> web_extension::Tabs {
        web_extension::Browser::tabs(&*web_extension::browser)
    }
    fn send_address_to_tabs(sender: JsValue) {
        let address = JsValue::from(zcashclient::Client::new().address());
        let cs_chan = MessageReceiver::get_console_script_channel();
        web_sys::console::log_1(
            &cs_chan.send_message(
                web_extension::MessageSender::from(sender)
                    .tab()
                    .expect("\"Active\" message was not sent from a tab")
                    .id()
                    .expect("Tab has nonexistent tab ID"),
                &address,
                None,
            ),
        )
    }
    fn register_cb_with_listener(self, runtime: &web_extension::Runtime) {
        runtime
            .on_message()
            .add_listener(self.callback.as_ref().unchecked_ref());
        self.callback.forget();
    }
}

#[wasm_bindgen]
pub async fn background() {
    console_error_panic_hook::set_once();
    fetch_test().await;
    let runtime = web_extension::Browser::runtime(&*web_extension::browser);
    web_sys::console::log_1(&"background is in".into());
    let receiver = MessageReceiver::new();
    receiver.register_cb_with_listener(&runtime);
    zcashclient::NoteMaker::create_note();
    let rngfb = zcashclient::RngFromBrowser::new();
    web_sys::console::log_1(&JsValue::from(format!(
        "{:?}",
        rngfb.random_values
    )));
}

#[wasm_bindgen]
pub async fn fetch_test() {
    web_sys::console::log_1(&JsValue::from(
        JsFuture::from(
            web_sys::Response::from(
                JsFuture::from(
                    web_sys::window()
                        .expect("No global window object")
                        .fetch_with_str("https://localhost:3030"),
                )
                .await
                .expect("Fetch rejected? I think?"),
            )
            .json()
            .expect("not a promise"),
        )
        .await
        .expect("not json"),
    ))
}
