#[allow(warnings)]
pub(crate) mod web_extension {
    use js_sys::{Function, Object, Promise};
    use mockall::automock;
    use std::ops;
    use wasm_bindgen::JsValue;

    pub struct Browser;
    impl ops::Deref for Browser {
        type Target = BrowserCore;
        fn deref(&self) -> &BrowserCore {
            &BrowserCore {}
        }
    }

    pub struct BrowserCore;
    #[automock]
    impl BrowserCore {
        pub fn runtime(&self) -> self::Runtime {
            Runtime {}
        }
    }

    pub static browser: Browser = Browser {};

    pub struct Runtime;
    #[automock]
    impl Runtime {
        pub fn send_message<'a>(
            &self,
            extension_id: Option<&'a str>,
            message: &wasm_bindgen::JsValue,
            options: Option<&'a js_sys::Object>,
        ) -> Promise {
            Promise::resolve(&JsValue::from("I'm a sent message"))
        }

        pub fn on_message(&self) -> Event {
            Event {}
        }
        pub fn get_url(&self, foo: &str) -> wasm_bindgen::JsValue {
            wasm_bindgen::JsValue::from("foo")
        }
    }

    pub struct Event;
    #[automock]
    impl Event {
        pub fn add_listener(&self, ear: &Function) {
            ear.call1(
                &JsValue::null(),
                &JsValue::from("I'm a received message!"),
            );
        }
    }
}
